package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import static sample.Controller.*;
import java.io.IOException;
public class Main extends Application{


    // ServerSocket serverSocket;

    @Override
    public void start(Stage primaryStage) throws Exception{
       Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Quiz Server");
//        Group root = new Group();
//        Scene scene = new Scene(root, 600, 330, Color.WHITE);
//
//        GridPane gridpane = new GridPane();
//        gridpane.setPadding(new Insets(5));
//        gridpane.setHgap(10);
//        gridpane.setVgap(10);
//
//        Label label = new Label("Label");
//        GridPane.setHalignment(label, HPos.CENTER);
//        gridpane.add(label, 0, 0);
//
//
//        root.getChildren().add(gridpane);
//        primaryStage.setScene(scene);
        primaryStage.setScene(new Scene(root,400,500));
        primaryStage.show();
        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.setDaemon(true); //terminate the thread when program end
        socketServerThread.start();
    }


    public static void main(String[] args)  {
        launch(args);
    }

    private class SocketServerThread extends Thread {

    @FXML
    private Label label;

        @Override
        public void run() {
            ArrayList<String> lines =  new ArrayList<>();
            ArrayList<String> correct_answers = new ArrayList<>();
            correct_answers.add("Warszawa");
            correct_answers.add("Nie");
            correct_answers.add("JS");
            // create a Scanner object for reading files
            try (Scanner reader = new Scanner(new File("questions.txt"))) {

                // read all lines from a file
                while (reader.hasNextLine()) {
                    lines.add(reader.nextLine());
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
                String host = "127.0.0.1"; // nazwa hosta
                int port = 50000; // numer portu
                try {
                    InetSocketAddress isa = new InetSocketAddress(host, port);
                    ServerSocket serverSock = new ServerSocket();
                    serverSock.bind(isa);
                    boolean serverIsRunning = true;

                    while (serverIsRunning) {
                        for (int i = 0; i < lines.size(); i++) {
                            System.out.println(lines.get(i));

//                            label.setText(lines.get(i));

                            Socket conn = serverSock.accept();
                            InputStream sockIn = conn.getInputStream();
                            BufferedReader in = new BufferedReader(new InputStreamReader(sockIn));

                            String received_data = in.readLine();
                            String[] parts = received_data.split("-");
                            String answer = parts[0];
                            String nick = parts[1];

                           while(!answer.equals(correct_answers.get(i))) {
                               System.out.println("Nadeszła błędna odpowiedź");
                               conn = serverSock.accept();
                               sockIn = conn.getInputStream();
                               in = new BufferedReader(new InputStreamReader(sockIn));
                               received_data = in.readLine();
                               parts = received_data.split("-");
                               answer = parts[0];
                               nick = parts[1];
                            }
                            System.out.println(nick + " odpowiedział poprawnie");

                        conn.close();
                        if(i==2)
                            {serverIsRunning = false;}
                    }
                    }
                    System.out.println("Odpowiedziano na wszystkie pytania");
                    serverSock.close(); // zamknięcie gniazda serwera
                } catch (IOException e) {
                    System.out.println("Błąd IO");
                }



        }

    }
}
