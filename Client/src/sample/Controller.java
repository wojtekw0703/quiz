package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller {

    @FXML
    private TextField answer_box;
    @FXML
    private TextField nick_box;

    @FXML   //KLIENT
    private void handleButtonAction(ActionEvent event) {
        try {
            String answer;
            String nick;

            String serverHost = "127.0.0.1";
            int serverPort = 50000;
            Socket socket = new Socket(serverHost, serverPort);
            OutputStream sockOut = socket.getOutputStream();
            InputStream sockIn = socket.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(sockIn));
            PrintWriter out = new PrintWriter(sockOut, true);

            answer = answer_box.getText();
            nick = nick_box.getText();
            answer_box.setText("");

            String joinedString = String.join("-", answer,nick);

            out.println(joinedString); // wysyłanie danych

            sockOut.close();
            sockIn.close();
            socket.close();
        } catch (UnknownHostException exc) {
            System.out.println("Nieznany host");
        } catch (SocketException exc) {
            System.out.println("Wyjątki związane z komunikacją przez gniazda ");
        } catch (IOException exc) {
            System.out.println("inne wyjątki we/wy");
        }
    }

    public void initialize(URL url, ResourceBundle rb) {
    }
}
